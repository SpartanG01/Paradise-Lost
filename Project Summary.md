# Paradise-Lost
Documentation and support for the "Paradise Lost" modpack for Minecraft 1.16.5 from GrayTeamGaming

What started out as a small collection of mods for some friends at work has turned into an endeavor to produce a playable modpack for Minecraft 1.16.5 that represents the essence of what Minecraft is while improving on and expanding on it's features and gameplay. This modpack does not include any futuristic or magical technology. No laser beams and teleporters. Believability was the primary focus when building it and the main goal of this modpack is to add features and expand technology in a way that still feels like it fits the Minecraft universe. The goal of this modpack is not to utterly trivialize gameplay but to encourage creative engaging gameplay.

To that end, this modpack features and is built around the "Create" and "Immersive Engineering" mod packs and includes many mods, addons, and additional custom resources designed to create a uniform experience and a sensible progression through technological tiers.

Visit the [Wiki](https://grayteammods.fandom.com/wiki/GrayTeamMods_Wiki) for more info!
